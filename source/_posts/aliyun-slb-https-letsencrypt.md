---
title: 阿里云容器服务的负载均衡HTTPS使用 Let's Encrypt 免费证书
date: 2018-08-12 11:51:33
tags: [阿里云,Docker,负载均衡,HTTPS]
---
#### 申请 Let's Encrypt 通配符证书
阿里云提供的SSL证书服务提供免费的单域名证书，而我们的负载均衡需要使用的是通配符域名，只有这样负载均衡服务器才能将不同的子域名映射到对应的容器
服务上。
![阿里云提供的SSL免费证书购买](http://c.trojx.me/image/20180812115517.png)
`Let's Encrypt`目前已经支持申请通配符证书。虞大胆的这篇博文[《Let's Encrypt 终于支持通配符证书了》](https://www.jianshu.com/p/c5c9d071e395)
已经介绍了申请`Let's Encrypt`通配符证书的具体做法(文中写到的各命令行中的`certbot-auto`应为`certbot`)。
值得注意的是，MacOS用户可以方便地通过HomeBrew安装`certbot`:
> brew install certbot

 安装好之后就可参照上述这篇文章进行证书的生成了。
#### 配置负载均衡
使用`certbot`客户端申请通配符证书后会在目录下生成四个文件：
- cert1.pem
- chain1.pem
- fullchain1.pem
- privkey1.pem

我们需要用到的是其中的证书`cert1.pem`和私钥`privkey1.pem`文件。
![证书文件](http://c.trojx.me/image/20180812123931.png)

进入负载均衡管理控制台，选择`Docker`集群所对应的负载均衡实例，为其添加一条监听规则。
![添加监听按钮](http://c.trojx.me/image/20180812124118.png)
前端协议及端口选择`HTTPS:443`，后端协议及端口选择`HTTP:9080`。
![添加监听](http://c.trojx.me/image/20180812125227.png)
在服务器证书处选择上传证书。
![创建证书](http://c.trojx.me/image/20180812124728.png)
将`cert1.pem`文件的内容填写至`公钥证书`处，将`privkey1.pem`文件内容填写至`私钥`处。
之后配置健康检查或者直接不使用健康检查，点击完成完成配置即可。
#### 效果展示
在使用HTTP方式访问站点时，浏览器会提示当前连接不安全。
![HTTP访问站点](http://c.trojx.me/image/20180812130414.png)
在经过上述配置后，即可通过负载均衡的自定义域名使用HTTPS访问了。文中可以看见站点所使用的证书为`Let's Encrypt`通配符证书。
![HTTPS访问站点](http://c.trojx.me/image/20180812125700.png)

#### 参考连接
- [《Let's Encrypt 终于支持通配符证书了》-虞大胆-简书](https://www.jianshu.com/p/c5c9d071e395)
- [《简单路由-HTTP 协议变为 HTTPS 协议》-帮助文档-阿里云](https://help.aliyun.com/document_detail/25987.html)