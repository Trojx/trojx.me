---
title: SnappyDb存储自定义对象时抛出异常 Maybe you tried to retrieve an array using this method ?
date: 2016-09-21 14:48:17
tags: [SnappyDb,异常]
---

使用`Snappy`做本地数据缓存时，遇到如下异常：
>E/ConfigUtil: com.snappydb.SnappydbException: Maybe you tried to retrieve an array using this method ? please use getObjectArray instead Class cannot be created (missing no-arg constructor): net.aimoqi.moqidump.bean.WeightBridge

两个类均作为`ArrayList`存储，分别是



```java
    public class WeightBridgeCam{
        private String ID;

    //    public WeightBridgeCam() {
    //    }

        public WeightBridgeCam(String ID) {
            this.ID = ID;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        @Override
        public String toString() {
            return "WeightBridgeCam{" +
                    "ID='" + ID + '\'' +
                    '}';
        }
    }

```

```java
    public class WeightBridge{
        private String id;
        private String name;
        private ArrayList<WeightBridgeCam> camList;

    //    public WeightBridge() {
    //    }

        public WeightBridge(String id, String name, ArrayList<WeightBridgeCam> camList) {
            this.id = id;
            this.name = name;
            this.camList = camList;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ArrayList<WeightBridgeCam> getCamList() {
            return camList;
        }

        public void setCamList(ArrayList<WeightBridgeCam> camList) {
            this.camList = camList;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            WeightBridge that = (WeightBridge) o;

            if (!id.equals(that.id)) return false;
            return name.equals(that.name);

        }

        @Override
        public int hashCode() {
            int result = id.hashCode();
            result = 31 * result + name.hashCode();
            return result;
        }
    }

```

百度了很久无果，也看了`SnappyDB`的Issues，并没有解决该问题。结果Google出一片[文章](http://klaas-loclet.tumblr.com/post/118370057170/using-snappydb-for-caching)
文中指出`SnappyDB`的一个好处就是存储的对象不用实现`Serializable`。但是必须保证存储的类遵循以下三个条件：

1. 类必须有一个默认的（无参）的构造函数(The class must have a default (no-arg) constructor.)
2. 类不能包含任何（非静态的）内部类(The class must not have any (non-static) inner classes.)
3. 类中引用的任何类同样必须遵循上述两个规则(Any classes referenced in fields of your model class must also obey 1. and 2.)

对于我之前遇到的问题，针对第1点，为每个类添加一个默认的构造函数即可解决此问题。
```java
     public class WeightBridgeCam{

        ...

        public WeightBridgeCam() {
        }
        ...

```
```java
     public class WeightBridge{

        ...

        public WeightBridge() {
        }
        ...

```