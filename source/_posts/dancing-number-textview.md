---
title: Android 数字跳动的TextView实现
date: 2017-01-19 21:40:14
tags: [Android,TextView,自定义]
---
### 简介
`DancingNumberView`是一个用于跳动显示文本中数字的控件,继承自TextView.这种控件一般用于显示金额等对用户较为敏感的数字,让UI交互更加生动.
它具有以下几点特性:
- 自动获取文本中的所有数字,并同时开始跳动,免去多个TextView拼接的麻烦
- 支持数字按照自定义的格式显示,例如限定只显示小数点后两位

### 效果预览
![http://cdn.trojx.me/blog_pic/android_dancing_number_view.gif](http://cdn.trojx.me/blog_pic/android_dancing_number_view.gif)

### 导入使用
#### Gradle
第1步,在project的build.gradle文件中适当位置添加

	allprojects {
		repositories {
			...
			maven { url "https://jitpack.io" }
		}
	}

第2步,在app的build.gradle文件中适当位置添加依赖项

	dependencies {
	       compile 'com.github.JianxunRao:DancingNumberView:V1.0.1'
	}

### 使用方式
#### 通过XML布局
    <me.trojx.dancingnumber.DancingNumberView
                android:id="@+id/dnv"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                app:dnv_duration="6000"
                app:dnv_format="%.2f"/>

#### 通过Java代码
    DancingNumberView dnv = (DancingNumberView) findViewById(R.id.dnv);
       dnv.setText(text);//设置显示内容
       dnv.setDuration(duration);//设置完成跳动的持续时长(单位ms)
       dnv.setFormat(format);//设置数字的显示格式
       dnv.dance();//启动效果,开始数字跳动

### 关键代码

```java
    /**
     * 文本中的数字开始跳动
     */
    public void dance() {

        text = getText().toString();
        numbers=new ArrayList<>();
        Pattern pattern = Pattern.compile("\\d+(\\.\\d+)?");
        Matcher matcher=pattern.matcher(text);
        while (matcher.find()){
            numbers.add(Float.parseFloat(matcher.group()));
        }
        textPattern = text.replaceAll("\\d+(\\.\\d+)?",PLACEHOLDER);
        numberTemp=new float[numbers.size()];

        ObjectAnimator objectAnimator=ObjectAnimator.ofFloat(this,"factor",0,1);
        objectAnimator.setDuration(duration);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.start();
    }

    /**
     * 获取算数因子
     * @return 算数因子
     */
    public float getFactor() {
        return factor;
    }

    /**
     * 设置算数因子,为ObjectAnimator调用
     * @see ObjectAnimator
     * @param factor 算数因子
     */
    public void setFactor(float factor) {
        String textNow=textPattern;
        this.factor = factor;
        for (int i=0;i<numberTemp.length;i++){
            numberTemp[i]=numbers.get(i)*factor;
            textNow=textNow.replaceFirst(PLACEHOLDER,String.format(format,numberTemp[i]));
        }
        setText(textNow);
    }
```
