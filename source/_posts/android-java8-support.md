---
title: Android支持Java8特性的Gradle配置
date: 2017-01-06 09:17:57
tags: [Android,Java8,Lambdas]
---

在project的build.gradle文件中添加:
```gradle
 buildscript {
        repositories {
            mavenCentral()
            jcenter()
            maven { url "https://jitpack.io" }
        }
        dependencies {
            classpath 'com.android.tools.build:gradle:2.2.0'
            classpath 'com.neenbedankt.gradle.plugins:android-apt:1.8'
            classpath 'me.tatarka:gradle-retrolambda:3.2.5'
        }
    }
```
在app的build.gradle文件中添加:
```gradle
apply plugin: 'com.android.application'
apply plugin: 'com.neenbedankt.android-apt'
apply plugin: 'me.tatarka.retrolambda'

android {
    compileSdkVersion xx
    buildToolsVersion "xx"

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
```

然后点击`Sync project`按钮即可.