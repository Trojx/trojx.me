---
title: Python解析音悦台MV视频地址与封面
date: 2017-01-13 10:40:22
tags: [Python,音悦台]
---
音悦台的MV网页链接是连续的,形如`http://v.yinyuetai.com/video/2770558`,最后面的id最多由七位数字组成.
参考网上[Java版本](http://iamyida.iteye.com/blog/2250181)的抓取代码,现用Python实现如下:

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/1/12 0012 下午 4:24
# @Author  : Trojx
# @File    : yinyuetai.py

import requests
import re


def get_mv_info(video_id):
    params = {'flex': True, 'videoId': video_id}
    res = requests.get('http://www.yinyuetai.com/insite/get-video-info', params)
    # print res.content
    qualities = ['sh', 'he', 'hd', 'hc']
    qualities_cn = ['VIP专享MV', '超清MV', '高清MV', '流畅MV']
    for q, q_cn in zip(qualities, qualities_cn):
        result = re.findall('.+(http://' + q + '.yinyuetai.com/uploads/videos/common/[a-zA-Z0-9]+\\.flv).+',
                            res.content)
        if len(result) > 0:
            print q_cn + ':' + result[0]
    covers = re.findall('.+(http://img[0-9].yytcdn.com/video/mv/[0-9]+/[0-9]+/.+\\.jpe?g).+', res.content)
    if len(covers) > 0:
        print '封面:' + covers[0]


if __name__ == '__main__':
    for i in range(2000000,2000010):
        print i
        get_mv_info(i)

```

解析结果如下:
```
2000000
超清MV:http://he.yinyuetai.com/uploads/videos/common/180E0144714AA06BDC7285DD738989A7.flv
高清MV:http://hd.yinyuetai.com/uploads/videos/common/AECB0144713DCF2CF7C41E940E7DF33A.flv
流畅MV:http://hc.yinyuetai.com/uploads/videos/common/67850144712E3ECA363E61435F9BFB7D.flv
封面:http://img4.yytcdn.com/video/mv/140227/2000000/94AB014471317FB865E22018482DA924_240x135.jpeg
2000001
超清MV:http://he.yinyuetai.com/uploads/videos/common/7E5B01447146F6EB4639EE6596685AB0.flv
高清MV:http://hd.yinyuetai.com/uploads/videos/common/8AE00144713DCF821E4342BB0FE7A848.flv
流畅MV:http://hc.yinyuetai.com/uploads/videos/common/815301447134A76953CCE2B49A59FB5F.flv
封面:http://img2.yytcdn.com/video/mv/140227/2000001/1F870144713968D3B501FFC592CDCA4F_640x360.jpeg
2000002
超清MV:http://he.yinyuetai.com/uploads/videos/common/9D4F01447146F7A5B400F2DA20D1C6A8.flv
高清MV:http://hd.yinyuetai.com/uploads/videos/common/5E15014471408F01EF0981F2C14E8080.flv
流畅MV:http://hc.yinyuetai.com/uploads/videos/common/C6B90144713592A0A63361B57D09BF67.flv
封面:http://img1.yytcdn.com/video/mv/161116/2724994/-M-b072a7fbe78fb03119343e23e250519b_120x67.jpg
2000003
超清MV:http://he.yinyuetai.com/uploads/videos/common/AC840144713FA4F95FE0234618F1408F.flv
高清MV:http://hd.yinyuetai.com/uploads/videos/common/A92D0144713FA49BAD665B3451B891B9.flv
流畅MV:http://hc.yinyuetai.com/uploads/videos/common/08430144713B10BE37872E2F32E7151A.flv
封面:http://img1.yytcdn.com/video/mv/140227/2000003/CEC80144713EC4CE1B5F929B1613919B_640x360.jpeg
2000004
2000005
超清MV:http://he.yinyuetai.com/uploads/videos/common/934901447149B6C9AE3ED95AB5FB5E26.flv
高清MV:http://hd.yinyuetai.com/uploads/videos/common/AD0601447146F9A444E46DE32026A744.flv
流畅MV:http://hc.yinyuetai.com/uploads/videos/common/C98E0144713DD15D8341D03D80F49785.flv
封面:http://img1.yytcdn.com/video/mv/140227/2000005/CCC001447141899186A8C71A2C4375B2_640x360.jpeg
2000006
超清MV:http://he.yinyuetai.com/uploads/videos/common/22A001447152DDC9EEED777CE966098D.flv
高清MV:http://hd.yinyuetai.com/uploads/videos/common/91A80144714B8B0AB0847C76595E0465.flv
流畅MV:http://hc.yinyuetai.com/uploads/videos/common/113E0144713B10541F990C1252484111.flv
封面:http://img2.yytcdn.com/video/mv/140227/2000006/A7FA014471443FD7D5BAE04A3E71A9B0_640x360.jpeg
2000007
超清MV:http://he.yinyuetai.com/uploads/videos/common/505401447151F36B43D5FED9577338B1.flv
高清MV:http://hd.yinyuetai.com/uploads/videos/common/DDDE0144714B8B65F7B31E41BF78FBAD.flv
流畅MV:http://hc.yinyuetai.com/uploads/videos/common/32D90144713FA3EC551665EDBDFA352B.flv
封面:http://img0.yytcdn.com/video/mv/140227/2000007/0F6401447142CE1F492539149F17F41F_240x135.jpeg
2000008
高清MV:http://hd.yinyuetai.com/uploads/videos/common/F5BF0144715CEFF0F03592414A0D63D3.flv
流畅MV:http://hc.yinyuetai.com/uploads/videos/common/A43E0144714437CB451A0915B478F2DA.flv
封面:http://img3.yytcdn.com/video/mv/140227/2000008/E1760144714E52CD72654AD7D3098840_640x360.jpeg
2000009
超清MV:http://he.yinyuetai.com/uploads/videos/common/5BB30144715CF0B39D86F44269C3BFC5.flv
高清MV:http://hd.yinyuetai.com/uploads/videos/common/A57E01447156880F8E546793186FD886.flv
流畅MV:http://hc.yinyuetai.com/uploads/videos/common/DCBF014471426740F2163ED265CCF40F.flv
封面:http://img4.yytcdn.com/video/mv/140227/2000009/33310144714E6152FCC531A4D2ECC4AD_640x360.jpeg
```