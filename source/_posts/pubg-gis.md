---
title: 绝地求生路线记录助手 v0.1.0
date: 2017-10-25 17:34:58
tags: [绝地求生,助手,PUBG]
---

![](https://img.shields.io/badge/%E5%85%8D%E8%B4%B9-%E6%98%AF-green.svg) 
![](https://img.shields.io/badge/%E6%97%A0%E7%97%85%E6%AF%92-%E6%98%AF-green.svg)
![](https://img.shields.io/badge/%E4%B8%AD%E6%96%87-%E6%98%AF-green.svg)
### 下载方式
- 本站下载 [点击下载](http://cdn2.trojx.me/%E7%BB%9D%E5%9C%B0%E6%B1%82%E7%94%9F%E8%B7%AF%E7%BA%BF%E8%AE%B0%E5%BD%95%E5%8A%A9%E6%89%8B.exe)
- 百度云盘 [http://pan.baidu.com/s/1pLjzqen](http://pan.baidu.com/s/1pLjzqen)

### 软件介绍
**绝地求生路线记录助手**是一款为绝地求生(Playerunknown's Battlegrounds)开发的辅助工具(非外挂性质).它的主要功能是帮助你记录每一局比赛的移动路线,
并生成直观的图片.

![http://cdn.trojx.me/blog_pic/composite_example.png](http://cdn.trojx.me/blog_pic/composite_example.png?imageView2/2/w/640/h/360)

本助手通过不断扫描位于游戏界面右下角的小地图,并由此通过图像匹配确定玩家在整个地图上的精确位置,从而绘制出玩家的行进路线.

![http://cdn.trojx.me/blog_pic/example_minimap.jpg](http://cdn.trojx.me/blog_pic/example_minimap.jpg?imageView2/2/w/640/h/360)

软件的使用方法主要分为两种:第一种是直接由视频文件生成路线图.通过指定游戏录像的视频文件,再点击开始按钮,你很快就能得到一幅路线图.

![http://cdn.trojx.me/blog_pic/example_from_video.png](http://cdn.trojx.me/blog_pic/example_from_video.png?imageView2/2/w/1280/h/720)

第二种方式是实时生成路线图.在你游戏的时候打开本软件并点击生成按钮,它将实时获取游戏界面,并由此绘制出这局游戏中你的路线图.

![http://cdn.trojx.me/blog_pic/example_from_live.png](http://cdn.trojx.me/blog_pic/example_from_live.png?imageView2/2/w/640/h/360)

它还附带一些简单的设置:首先是更改路径颜色.顾名思义,你可以自定义绘制的路线图上的路径采用哪种颜色.

![http://cdn.trojx.me/blog_pic/change_path_color.png](http://cdn.trojx.me/blog_pic/change_path_color.png?imageView2/2/w/1280/h/720)

然后是路径线条宽度.同样,你也可以指定路线图上路径的显示宽度.你还可以设定采样间隔.对于电脑性能不太高的用户来说,适当提高采样间隔将会是一个明智之选.

![http://cdn.trojx.me/blog_pic/select_path_thickness.png](http://cdn.trojx.me/blog_pic/select_path_thickness.png?imageView2/2/w/640/h/360)

最后,当你勾选了输出整张地图这个选项时,无论你整场游戏的路径就是一个小红点(是的,就是说你呢,厕所里的LYB),还是说你已经在地图上玩起了GTA5,路线记录助手都将输出一张全地图范围的路线图.

![http://cdn.trojx.me/blog_pic/change_path_style_full_map.png](http://cdn.trojx.me/blog_pic/change_path_style_full_map.png?imageView2/2/w/800/h/800)

虽然这款软件不能像那些收费的辅助软件一样让你横行整个地图.但是记录下你每一局的行动路线,有助于你在以后的游戏中更加懂得规划好行进路线,并由此提升吃鸡概率.同时,希望这一功能对职业战队的日常训练与战术制定能起到一些作用.
那最后就祝大家"大吉大利,晚上吃鸡"吧!


