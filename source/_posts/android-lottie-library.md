---
title: Lottie安卓开源动画库使用
date: 2017-02-06 17:03:34
tags: [Lottie,Android,Library]
---

### 碉堡的Lottie

Airbnb最近开源了一个名叫Lottie的动画库,它能够同时支持iOS,Android与ReactNative的开发.此消息一出,还在苦于探索自定义控件各种炫酷特效的我,兴奋地就像发现的新大陆一般.可以说,Lottie的出现,将极大地解放Android/iOS工程师于无尽的编写原生自定义动画的工作中.
当我们的项目中用GIF实现一些复杂的视觉效果的时候,会遇到许多的问题.比如,GIF的文件过于庞大,并且对于不同分辨率设备的适配存在不便,并且Gif格式的色深问题是一个死穴.

比如下面这几个动画效果:

![http://cdn.trojx.me/blog_pic/Example1.gif](http://cdn.trojx.me/blog_pic/Example1.gif)

还有这些:

![http://cdn.trojx.me/blog_pic/Example2.gif](http://cdn.trojx.me/blog_pic/Example2.gif)

设计这些动画效果显然不是写代码的程序员应该负责的事情.那有没有什么办法,能让美工在AE软件上设计的动画直接用于移动端呢?
有的,那就是使用Lottie.

![http://cdn.trojx.me/blog_pic/lottie_sum.png](http://cdn.trojx.me/blog_pic/lottie_sum.png)

如上图所示,通过安装在AE上的一款名叫bodymovin的插件,能够将AE中的动画工程文件转换成通用的json格式描述文件,bodymovin插件本身是用于在网页上呈现各种AE效果的一个开源库,lottie做的事情就是实现了一个能够在不同移动端平台上呈现AE动画的方式.从而达到动画文件的**一次绘制、一次转换、随处可用**的效果.
当然,就如Java`一次编译,随处运行`一样,`lottie`本身这个动画播放库并不是跨平台的.

### 上手使用
说了那么多,下面来详细说说怎样使用这个碉堡的库.首先声明,以下涉及到的软件可能包含破解版,如果你资金充裕,请支持正版.
以下使用方式与软件在2017年2月6日都有效.

#### 安装Adobe After Effects CC 2017 
Adobe是个好公司,做了很多牛逼的软件,但是无一例外都被国人破解了.本例使用的是最新版的AE CC 2017.

![http://cdn.trojx.me/blog_pic/AE_CC_2017.png](http://cdn.trojx.me/blog_pic/AE_CC_2017.png)

- 可用下载地址 http://www.dayanzai.me/after-effects-cc-2014.html
- CDN 分流地址 http://trojx-me.oss-cn-hangzhou.aliyuncs.com/AE%202017%5BWwW.DaYanZai.Me%5D.rar

感谢作者提供软件.注意此版本**只适用于WIN 64位系统**,笔者在 Win7 64 位
环境下能够正常安装并使用.安装过程中按照内置说明安装即可.

#### 安装bodymovin插件

想了解此插件可以参看该插件的[GitHub页面](https://github.com/bodymovin/bodymovin).

##### 下载`bodymovin.zxp`插件包 
此文件位于工程中的[`/build/extension/`](https://github.com/bodymovin/bodymovin/blob/master/build/extension/bodymovin.zxp)目录下,如果外网速度慢可以[从这里](http://cdn.trojx.me/blog_raw/bodymovin.zxp)可以下载该插件的最新版本.

##### 安装插件 
项目说明中给出了为AE安装插件的三种方式:
- 通过第三方软件ZXP Installer安装;
- 手动安装;
- 使用[Adobe官方插件安装器](https://helpx.adobe.com/x-productkb/global/installingextensionsandaddons.html)安装.

这三种笔者都试过,最后得出只有第二种(也是看起来最繁琐的)有效.这里详细说明一下第二种方法:

- 先关闭AE;
- 用WinRAR或类似软件打开`bodymovin.zxp`文件,并将解压后的文件夹直接复制到`C:\Program Files (x86)\Common Files\Adobe\CEP\extensions`
或者是`C:<username>\AppData\Roaming\Adobe\CEP\extensions`下,对于MAC机器路径是`/Library/Application\ Support/Adobe/CEP/extensions/bodymovin`
![http://cdn.trojx.me/blog_pic/bodymovin_zxp_extracted.png](http://cdn.trojx.me/blog_pic/bodymovin_zxp_extracted.png)
![http://cdn.trojx.me/blog_pic/bodymovin_zxp_extracted_copy.png](http://cdn.trojx.me/blog_pic/bodymovin_zxp_extracted_copy.png)
- 修改注册表.对于Windows,打开注册表修改器,找到`HKEY_CURRENT_USER/Software/Adobe/CSXS.6`,并在此路径下添加一个名为`PlayerDebugMode`的KEY,
并赋值为1;对于MAC,打开文件`~/Library/Preferences/com.adobe.CSXS.6.plist`并在末尾添加一行,键为`PlayerDebugMode`,值为1.
- 设置AE 无论以何种方式安装bodymovin插件,都需要在AE的`编辑->首选项->常规`中勾选`允许脚本写入文件和访问网络`(默认不开启)
![http://cdn.trojx.me/blog_pic/ae_setting.png](http://cdn.trojx.me/blog_pic/ae_setting.png)

#### 开始制作动画
由于笔者目前不会使用AE(废话,软件都是刚装的),这里我们打开一个现有的工程文件.
[从这里](https://github.com/airbnb/lottie-android/tree/master/After%20Effects%20Samples)可以找到一些Lottie中演示过的动画的AE源文件,下载到本地后在AE中打开即可.这里我们选用`EmptyState.aep`这个实例工程,稍作修改:

![http://cdn.trojx.me/blog_pic/empty_page_editting.png](http://cdn.trojx.me/blog_pic/empty_page_editting.png)

#### 导出json数据
如果上文的`bodymovin`插件安装成功的话,在AE中的`窗口->拓展`中是能够找到它的.

![http://cdn.trojx.me/blog_pic/ae_bodymovin_menu.png](http://cdn.trojx.me/blog_pic/ae_bodymovin_menu.png)

在插件窗口中选择json数据文件导出的路径,点击`Render`按钮即可渲染工程并导出.

![http://cdn.trojx.me/blog_pic/ae_bodymovin_render.png](http://cdn.trojx.me/blog_pic/ae_bodymovin_render.png)

- 原始工程动画效果:
![http://cdn.trojx.me/blog_pic/empty_state_origin.gif](http://cdn.trojx.me/blog_pic/empty_state_origin.gif)

- 原始工程导出的json文件:http://cdn.trojx.me/blog_raw/lottie_data_origin.json

- 修改后工程动画效果:
![http://cdn.trojx.me/blog_pic/empty_state_edit.gif](http://cdn.trojx.me/blog_pic/empty_state_edit.gif)

- 修改后工程导出的json文件:http://cdn.trojx.me/blog_raw/lottie_data_edit.json

#### 使用Lottie库播放动画
终于说到主角了,然而关于它的使用方式却是相对简单的.Lottie的引入与使用就如其他库一样,这里以Android平台的使用为例.

在项目的build.gradle文件中加入:
```gradle
    dependencies {  
      compile 'com.airbnb.android:lottie:1.0.1'
      ...
    }
```
Lottie支持Jellybean (API 16)及以上的系统,最简单的使用方式是直接在布局文件中添加:

```xml
    <com.airbnb.lottie.LottieAnimationView
            android:id="@+id/animation_view"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:lottie_fileName="hello-world.json"
            app:lottie_loop="true"
            app:lottie_autoPlay="true" />
```
或者,你也可以通过代码的方式添加.比如从位于`app/src/main/assets`路径下的json文件中导入动画数据:
```java
    LottieAnimationView animationView = (LottieAnimationView) findViewById(R.id.animation_view);
    animationView.setAnimation("hello-world.json");
    animationView.loop(true);
```
这方法将在后台线程异步加载数据文件,并在加载完成后开始渲染显示动画.
如果你想复用加载的动画,例如下一个ListView中每一项都需要显示这个动画,那么你可以这么做:

```java
    LottieAnimationView animationView = (LottieAnimationView) findViewById(R.id.animation_view);
    ...
    LottieComposition composition = LottieComposition.fromJson(getResources(), jsonObject, (composition) -> {
        animationView.setComposition(composition);
        animationView.playAnimation();
    });
```

你还可以通过API控制动画,并且设置一些监听:

```java
    animationView.addAnimatorUpdateListener((animation) -> {
        // Do something.
    });
    animationView.playAnimation();
    ...
    if (animationView.isAnimating()) {
        // Do something.
    }
    ...
    animationView.setProgress(0.5f);
    ...
    // 自定义速度与时长
    ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f)
        .setDuration(500);
    animator.addUpdateListener(animation -> {
        animationView.setProgress(animation.getAnimatedValue());
    });
    animator.start();
    ...
    animationView.cancelAnimation();
```
在使用遮罩的情况下,`LottieAnimationView` 使用 `LottieDrawable`来渲染动画.如果需要的话,你可以直接使用drawable形式:

```java
    LottieDrawable drawable = new LottieDrawable();
    LottieComposition.fromAssetFileName(getContext(), "hello-world.json", (composition) -> {
        drawable.setComposition(composition);
    });
```

如果你需要频发使用某一个动画,可以使用`LottieAnimationView`内置的一个缓存策略:
`LottieAnimationView.setAnimation(String, CacheStrategy)`
其中`CacheStrategy`的值可以是`Strong`,`Weak`或者`None`,它们用来决定`LottieAnimationView`对已经加载并转换好的动画持有怎样形式的引用(强引用/弱引用).

### 补充
lottie在iOS中的使用介绍可以参看[陳董DON的文章](http://ios.devdon.com/?p=241&from=trojx.me)

分享一个能够在浏览器中[预览json动画数据的网站](http://svgsprite.com/demo/bm/player.php?render=canvas&bg=fff)

Lottie官方给的[Android Demo安装包](http://cdn.trojx.me/blog_raw/LottieSample-debug.apk)使用它能够查看示例动画,并能够载入并播放来自本地存储或网络的json动画数据.

好了,先写到这里,我去研究AE去了~后续应该会补上一个使用Lottie的Android Demo.