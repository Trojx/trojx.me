---
title: Hexo安装的几个注意点
date: 2016-09-14 15:24:38
tags: [hexo,备忘,图床,FTP部署]
---

>`Wordpress`至今没有找到对MD支持良好的插件，知道`hexo`很久了。但是一直没有上手使用过。
近日打算试试使用后者，没想到非常的易于操作，速度也快到爆炸。几个hexo命令就能生成一个简洁优雅的静态博客，着实令人心动。

这个博客工程在本地NodeJS环境的Windows7机器上生成，并且进行预览。静态文件部署在阿里云服务器上，这是[首页](http://www.trojx.me)。

在此记录下安装、配置hexo时遇到的几个关键点:


#### 使用Next主题

这是目前使用率最高的主题，[说明文档](http://theme-next.iissnan.com/)非常的详尽，照着它来就行了。
使用时唯一遇到的问题就是本地使用`hexo g`生成静态文件，再部署到服务器上时，会出现效果不一致的情况。这时需要使用`hexo clean`
清除所有生成的静态文件，重新生成并部署，就能解决。

#### 使用七牛图床
[qiniu4blog](https://github.com/wzyuliyang/qiniu4blog)可以使用七牛云存储创建自己的图床,用于写博客。
需要Python 2.7环境。安装方法 `pip install qiniu4blog`
这是配置文件指明了七牛存储bucket与密钥：
```
[config]
bucket = blog
accessKey = B-aqeCkTeD6K***********F-WxQ_sSkK9M
secretKey = cXfSWeQ_x__SY***********xJAKKLIgWohEbH
path_to_watch = D:\WebstormProjects\trojx.me\source\uploads

[custom_url]
enable = true
addr = http://cdn.trojx.me/
```

使用方式有两种：
##### 监听模式
打开终端
> qiniu4blog  #将会监听path_to_watch内的文件变动，上传图片

##### 命令行模式
> qiniu4blog d:/image1.jpg d:/image2.jpg d:/image3.jpg  #指定上传多个文件
脚本上传图片后，会将图片路径一并输出至`MARKDOWN_FORMAT_URLS.txt`文件中。每一行就是一个Markdown格式的图片链接，十分方便引用。

#### FTP部署
hexo 的ftp部署模块ftpsync在windows机器上出现了一些问题。导致无法连接至FTP服务器。但是它的作用也仅仅是将生成好的静态文件上传到服务器而已。
因此可以通过python脚本，指定public文件路径，每次部署时将静态文件全部上传即可，对同名文件覆盖处理。

```python
import sys
import os
import json
from ftplib import FTP

_XFER_FILE = 'FILE'
_XFER_DIR = 'DIR'

class Xfer(object):
    '''''
    @note: upload local file or dirs recursively to ftp server
    '''
    def __init__(self):
        self.ftp = None

    def __del__(self):
        pass

    def setFtpParams(self, ip, uname, pwd, port = 21, timeout = 60):
        self.ip = ip
        self.uname = uname
        self.pwd = pwd
        self.port = port
        self.timeout = timeout

    def initEnv(self):
        if self.ftp is None:
            self.ftp = FTP()
            print '### connect ftp server: %s ...'%self.ip
            self.ftp.connect(self.ip, self.port, self.timeout)
            self.ftp.login(self.uname, self.pwd)
            print self.ftp.getwelcome()

    def clearEnv(self):
        if self.ftp:
            self.ftp.close()
            print '### disconnect ftp server: %s!'%self.ip
            self.ftp = None

    def uploadDir(self, localdir='./', remotedir='./'):
        if not os.path.isdir(localdir):
            return
        self.ftp.cwd(remotedir)
        for file in os.listdir(localdir):
            src = os.path.join(localdir, file)
            if os.path.isfile(src):
                self.uploadFile(src, file)
            elif os.path.isdir(src):
                try:
                    self.ftp.mkd(file)
                except:
                    sys.stderr.write('the dir is exists %s'%file)
                self.uploadDir(src, file)
        self.ftp.cwd('..')

    def uploadFile(self, localpath, remotepath='./'):
        if not os.path.isfile(localpath):
            return
        print '+++ upload %s to %s:%s'%(localpath, self.ip, remotepath)
        self.ftp.storbinary('STOR ' + remotepath, open(localpath, 'rb'))

    def __filetype(self, src):
        if os.path.isfile(src):
            index = src.rfind('\\')
            if index == -1:
                index = src.rfind('/')
            return _XFER_FILE, src[index+1:]
        elif os.path.isdir(src):
            return _XFER_DIR, ''

    def upload(self, src):
        filetype, filename = self.__filetype(src)

        self.initEnv()
        if filetype == _XFER_DIR:
            self.srcDir = src
            self.uploadDir(self.srcDir)
        elif filetype == _XFER_FILE:
            self.uploadFile(src, filename)
        self.clearEnv()


if __name__ == '__main__':
    srcDir = r"./public"
    xfer = Xfer()
    xfer.setFtpParams('121.41.108.5', 'blog', 'rjx336617')  #指定FTP连接参数
    xfer.upload(srcDir)
```