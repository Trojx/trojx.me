---
title: Tinker 热修复框架 简单上手教程
date: 2016-09-28 01:08:15
tags: [Tinker,热修复,笔记]
---
#### 导言
前不久,腾讯推出了"微信小程序"这一概念,对移动原生应用的影响可谓巨大.而几乎就在同时,
腾讯在GitHub上开源了第一个项目[Tinker](https://github.com/Tencent/tinker),
这是一个Android平台的应用热修复框架.可以在不重新安装应用的情况下,对应用的代码/库
/资源进行更新.相关信息官方也已经给出详细介绍.
得到这个消息时,笔者也尝试学习它给的Sample,希望能够使用到自己的项目中去.毕竟热修补是
如今原生应用的一大痛点,有了热修补技术,类React Native应用的优势将缩小.
**这是一篇Tinker简单上手的说明文字,演示如何运行官方的Sample,仅作互相学习交流**
#### 导入Sample工程
将官方给出的[Sample工程](https://github.com/Tencent/tinker/tree/dev/tinker-sample-android)
在AndroidStudio中打开.首先一点,在app的build.gradle文件中找到`tinkerId = getTinkerIdValue()`
并将其替换成`tinkerId = "tinkerId"`,其中后面的值可以随意设置.再替换`ignoreWarning = false`
为`ignoreWarning = true`.(见图1)
![图1](http://cdn.trojx.me/tinker-sample-introduction/1.png)
#### 编译运行原版apk
按照往常操作一样,编译打包debug apk并安装.此时Tinker会在工程的`app/build/bakApk/`目录下
保存打包好的apk文件,找到刚才生成的apk文件,复制其完整文件名,在app的build.gradle文件找到
`tinkerOldApkPath`这一项设置,并将其设置为`tinkerOldApkPath = "${bakPath}/<刚才生成的apk文件名>"`(见图2)
![图2](http://cdn.trojx.me/tinker-sample-introduction/2.png)

#### 修改源码 生成新版apk 补丁
在`MainActivity.java`中,我们稍作改动,例如将`R.string.test_resource`对应的字符串资源的值修改(见图3),
```
    <!--<string name="test_resource">I am in the base apk</string>-->//原值
    <string name="test_resource">I am in the patch apk</string>//新值
```

还可以在`MainActivity`中添加一行代码(见图3)
```
    Log.e(TAG, "i am on patch onCreate");
```
![图3](http://cdn.trojx.me/tinker-sample-introduction/3.png)
你可以自行做出更多的改动(这也是我们需要热修复的原因),然后再Gradle脚本中找到'app:/tinker/tinkerPatchDebug'这条命令(见图4),
双击运行,它将生成debug版的patch(补丁)apk文件.

![图4](http://cdn.trojx.me/tinker-sample-introduction/4.png)

运行完毕后,Tinker会告诉你生成的补丁apk文件所在位置.(见图5,6)
![图5](http://cdn.trojx.me/tinker-sample-introduction/5.png)
![图6](http://cdn.trojx.me/tinker-sample-introduction/6.png)
将`patch_signed_7zip.apk`这个文件拷贝到Android设备的`ExternalStorageDirectory()`路径下.
文件的路径可以随意设定,只需在`MainActivity`中指明补丁Apk路径即可.(见图7)
![图7](http://cdn.trojx.me/tinker-sample-introduction/7.png)


#### 安装热修复补丁 观察程序变化
点击APP主界面中的`LOAD PATCH`加载补丁,提示成功后,点击`KILL SELF`结束当前进程,
重新启动后,即可发现变化.可以看到,经过上述修改,原有Apk的应用启动后在Log中打印为(见图8):
![图8](http://cdn.trojx.me/tinker-sample-introduction/8.png)

而应用补丁后,打印结果为(见图9):
![图9](http://cdn.trojx.me/tinker-sample-introduction/9.png)

而这一切就是应用了热修复的结果.

万事开头难,有了如此强大的框架,相信广大开发者开发的原生应用将更加强大.
