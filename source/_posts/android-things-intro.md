---
title: Android Things简介:它真的来了!
date: 2016-12-16 00:46:51
tags: [Android,IOT,Android Things]
---

谷歌已经开发了多款操作系统,而Android Things则是她最新发布的一款.
谷歌为智能手机与平板电脑提供Android OS;为可穿戴设备提供Android Wear 系统,例如智能手表;
为笔记本与台式机提供ChromeOS;为机顶盒与电视机提供Android TV系统...而现在,
Android Things将为智能硬件设备和物联网(Internet of Things,IoT)配件提供操作系统.
然而,从技术上的角度上说,Android Things 这款操作系统并不是一个新技术.
一下这些,就是你需要了解到的关于谷歌这款最新操作系统的各种信息:

![http://cdn.trojx.me/blog_pic/android-things.jpg](http://cdn.trojx.me/blog_pic/android-things.jpg)

#### 什么是Android Things?
就在2016年12月13日,谷歌公布了一个叫做Android Things的Android版本.这款操作系统可以运行在
有线扬声器、智能控温器、安全摄像头、路由器等各种设备上.谷歌的理念是,通过Android Things让Android开发者与公司
能够以他们过去的开发方式继续开发物联网硬件设备.
这其中的关键一点是Android Things 是Android系统的一个精简版,它定位于装备在智能硬件上,而非类似普通节能灯一样的设备.

#### 那Brillo又是什么?
值得注意的是,Android Things这款操作系统并不是真正的新系统.确切的说,它只是一个新的名字.就在去年,谷歌发布了Brillo这款
基于Android的面向智能硬件的操作系统.只不过在那之后就将此计划搁置一边了.Android Things可以看做是Brillo的"重命名"版本.
但前者的确做了很多升级与改进,比如允许开发者以开发Android标准平台的应用的方式与工具来开发智能硬件.这一点是Brillo没有实现的.
Brillo没有被广泛流传与使用是因为有一定的经验的开发者们发现使用一个全新的工具与平台来做智能设备开发是一件困难的事情.现在好了,
有了Android Things 提供的熟悉的开发工具,开发者们只需要考虑他们能有多快的速度开始开发了.

#### 有开发者预览版吗?
有的.谷歌已经发布了Android Things的开发者预览版.你可以[从这里](https://developer.android.com/things/preview/index.html)了解更多信息(包括Android Things的一些关键概念).

#### 还有其他需要了解的吗?
不要将Android Things理解成像Android或Android Wear一样的能运行在手机或手表上的操作系统.因为Android Things是一个运行在后台并不可见的操作系统.
它可以让智能设备处理一些自己的任务而非全让服务器做处理.正因为它能适应更复杂的任务,它的定位即是集成度更高的智能设备,例如打印机与门禁系统,而非那些普通的插座.

除此之外,Android Things 的设备还具备通过Weave与Android与iOS进行集成的能力.Weave是一款通讯协议系统.去年谷歌将其与Brillo一同发布.
这项协议将同样允许搭载Android Things的设备访问Google Assistant,[从这里](http://www.pocket-lint.com/news/137722-what-is-google-assistant-how-does-it-work-and-when-can-you-use-it)可以了解更多关于这项技术的信息.

#### Android Things 何时来到?
谷歌已经发布了这个系统的开发者预览版,然而并没有指名公布任何合作商.所以目前我们无法确定市场上出现的第一款Android Things设备将会是什么.就让我们拭目以待吧!
