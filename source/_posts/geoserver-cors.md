---
title: GeoServer 开启CORS跨域请求访问 
date: 2018-08-07 14:59:11
tags: [GeoServer,CORS,Tomcat]
---

#### GeoServer官方文档的方法
`GeoServer`的[官方文档](http://docs.geoserver.org/latest/en/user/production/container.html#enable-cors)中有给出`GeoServer`
开启`CORS`跨域请求访问的方法，即在`webapps/geoserver/WEB-INF/web.xml`路径下将以下内容取消注释:

```xml
<web-app>
  <!--...-->
  <filter>
      <filter-name>cross-origin</filter-name>
      <filter-class>org.eclipse.jetty.servlets.CrossOriginFilter</filter-class>
  </filter>
  <filter-mapping>
      <filter-name>cross-origin</filter-name>
      <url-pattern>/*</url-pattern>
  </filter-mapping>
  <!--...-->
</web-app>
```

#### 基于Tomcat容器的方法
实际使用过程中我们可能会遇到一个问题，正如官方文档中所说，独立安装版的`GeoServer`是基于`Jetty`这个应用容器的，而如果`GeoServer`是运行在其他应用容器中
（比如`Apache Tomcat`），则上述做法会使得`GeoServer`无法运行，控制台报错：
>SEVERE [localhost-startStop-1] org.apache.catalina.core.StandardContext.startInternal One or more Filters failed to 
start. Full details will be found in the appropriate container log file

大致意思就是说有一个`Filter`无法正常启动，导致应用无法正常运行。

我所使用的`GeoServer`是基于`Tomcat`自行构建的，因此开启`CORS`的方式会有一些不同。具体做法是在`webapps/geoserver/WEB-INF/web.xml`
中添加以下内容：
```xml
<web-app>
    <!--...-->
    <filter>
      <filter-name>CorsFilter</filter-name>
      <filter-class>org.apache.catalina.filters.CorsFilter</filter-class>
      <init-param>
        <param-name>cors.allowed.origins</param-name>
        <param-value>*</param-value>
      </init-param>
    <init-param>
      <param-name>cors.allowed.methods</param-name>
      <param-value>GET,POST,HEAD,OPTIONS,PUT</param-value>
    </init-param>   
    </filter>
    <filter-mapping>
      <filter-name>CorsFilter</filter-name>
      <url-pattern>/*</url-pattern>
    </filter-mapping>
    <!--...-->
</web-app>
```

官方文档写得没有错，但是没有考虑到其他的情况。而我盲目地套用官方文档的做法，不分清实际情况，使得被这个问题所困扰。

#### 参考文章
- [CORS * for GeoServer with Tomcat](https://gist.github.com/essoen/91a1004c1857e68d0b49f953f6a06235)
- [Can't access GeoServer after CORS filter](https://gis.stackexchange.com/questions/220238/cant-access-geoserver-after-cors-filter)
- [CORS - Tomcat - Geoserver](https://stackoverflow.com/questions/22363192/cors-tomcat-geoserver)
- [CORS on Tomcat](https://enable-cors.org/server_tomcat.html)