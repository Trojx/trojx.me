---
title: 使用阿里云容器镜像服务构建SpringBoot应用
date: 2018-07-23 15:34:42
tags: [docker,阿里云,容器镜像,SpringBoot]
---


####  传统方法的缺点
网上有很多关于使用Docker部署SpringBoot应用的教程，但是无一例外地使用了本地的Docker构建镜像，然后通过push本地镜像的方式提交至阿里云容器服务。
类似教程如：

- [Spring Boot 2.0(四)：使用 Docker 部署 Spring Boot - 纯洁的微笑博客](http://www.ityouknow.com/springboot/2018/03/19/spring-boot-docker.html)

- [用 Docker 构建、运行、发布一个 Spring Boot 应用](https://waylau.com/docker-spring-boot/)

按照如此方法，不仅需要引入并配置使用`Spring Boot Maven plugin`这个插件，还需要在本地机器安装Docker，然后执行`mvn package docker:build`。
构建好镜像后，再push到远程镜像仓库中。这样不仅每一次更新都要重新构建、重新push，而且极度消耗本地机器性能。

####  使用阿里云镜像服务构建
阿里云提供的镜像服务中，可以通过源代码直接构建，只需指明代码中的`Dockerfile`路径即可。由于其镜像构建服务可以使用海外机器构建，因此在构建maven项目时，
不会存在麻烦的仓库更新缓慢的问题。

具体做法如下：

- 在`SpringBoot`项目根路径下创建`DockerFile`：
![](http://c.trojx.me/image/20180723160925.png)

- 编辑`Dockerfile`，这里使用官方的[`maven`](https://hub.docker.com/_/maven/)镜像作为基础镜像：

```dockerfile
FROM maven:3.5.4-alpine
ADD . /app
WORKDIR /app/
RUN mvn clean package
EXPOSE 8080
ENTRYPOINT java -Djava.security.egd=file:/dev/./urandom -jar target/tim-sign-fys-0.0.1-SNAPSHOT.jar
```
> 注意：将`target/tim-sign-fys-0.0.1-SNAPSHOT.jar`替换为你的工程打包好的.jar包路径。

- 上传至代码仓库后，使用阿里云镜像服务进行构建：
![](http://c.trojx.me/image/20180723161821.png)

- 进入镜像管理，`立即构建`镜像：
![](http://c.trojx.me/image/20180723162216.png)

- 进入容器服务控制台，使用构建好的镜像创建应用：
![](http://c.trojx.me/image/20180723162522.png)

- 创建成功，SpringBoot应用自动启动：
![](http://c.trojx.me/image/20180723162700.png)

- 如果配置了路由，则可以很方便地从公网访问刚刚创建的SpringBoot应用：
![](http://c.trojx.me/image/20180723162846.png)
![](http://c.trojx.me/image/20180723162950.png)
> 因为此示例项目没有监听根路径`'/'`，因此访问根路径报错`Whitelabel Error Page`，但是这正好证明SpringBoot应用已经部署成功。


通过这种方法部署SpringBoot应用到阿里云容器服务上简单、高效。如果你的应用不想运行在阿里云容器服务中，同样可以使用阿里云容器镜像服务远程构建你的
镜像。然后将构建好的镜像部署到你指定的任意容器中即可。