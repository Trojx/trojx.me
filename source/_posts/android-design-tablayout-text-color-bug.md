---
title: TabLayout.setTabTextColors()的一个Bug
date: 2017-01-06 11:22:37
tags: [Android,TabLayout,Bug]
---

```java
tabLayout.setTabTextColors(getResources().getColor(R.color.colorSecondaryText),getResources().getColor(R.color.colorPrimary));
```
这行代码在使用`com.android.support:design:23.3.0`以上版本的依赖时(`25.*.*`经过验证),不能达到预期效果,表现形式为滑动ViewPager时tabText
的颜色能根据viewpager的position改变颜色,但是如果手动点击某一个tab,之前已选中的某一个tab的textcolor并不会改变回unselected color.
相同代码在使用`23.3.0`版本时效果正常.

解决办法:
用到viewpager的地方 不使用25+(或24+)版本的`support design library` 具体问题原因有待后续源码研究.

参考:

- [Tablayout's text color does not change when setting the selected item in code](http://stackoverflow.com/questions/35597445/tablayouts-text-color-does-not-change-when-setting-the-selected-item-in-code)

- [viewpager setCurrentItem not changing tablayout selected color](http://stackoverflow.com/questions/35150730/viewpager-setcurrentitem-not-changing-tablayout-selected-color)