---
title: Android Travis CI与fir.im、GitHub集成
date: 2016-11-19 13:41:24
tags: [Travis,Android]
---

#### build.gradle配置
注意是在app的build.gradle中配置,以下是一个实例:
```
android {
    compileSdkVersion 23
    buildToolsVersion "23.0.3"

    signingConfigs {
        releaseConfig {
            storeFile file("../keys/imoqi.jks")//指定密钥文件的路径
            storePassword project.hasProperty("KEYSTORE_PASS") ? KEYSTORE_PASS : System.getenv("KEYSTORE_PASS")//指定密钥参数
            keyAlias project.hasProperty("ALIAS_NAME") ? ALIAS_NAME : System.getenv("ALIAS_NAME")
            keyPassword project.hasProperty("ALIAS_PASS") ? ALIAS_PASS : System.getenv("ALIAS_PASS")
        }
    }
    buildTypes {
            release {
                minifyEnabled false
                proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
                signingConfig signingConfigs.releaseConfig//注意这一行
            }
    }
    ...
}
```
注意在windows平台中,对jks文件进行openssl加密再上传至Travis-ci中时,会导致Travis构建时无法正确解密,因此这里直接上传jks密钥,但是工程的
gradle.properties文件不添加进git,jks的密码信息存储在此,本地构建签名打包时通过project.hasProperty("KEYSTORE_PASS")等方法获取(见上文
代码),而在Travis中构建时通过设置环境变量而获取,如System.getenv("KEYSTORE_PASS"),这种做法一定程度上保证了安全性.
![Travis 环境变量设置](http://cdn.trojx.me/blog_pic/travis-env-setting.jpg)


#### .travis.yml 配置
在Android project的根目录下新建.travis.yml文件,典型的内容如下
```
    language: android 
    sudo: false
    git:
      depth: 3 #将git depth设置相对小的值,加快构建速度
    jdk:
    - oraclejdk8 #lambda表达式支持,如果工程中使用了rxJava等类库时添加
    android:
      components:
      - tools
      - build-tools-23.0.3
      - android-23
      - extra-android-m2repository
      - extra-android-support
    script:
    - "./gradlew assembleRelease"
    before_install:
    - chmod +x gradlew
    - gem install fir-cli #安装fir命令行客户端
    deploy:
      provider: releases
      api_key:
        secure: O2o06fEDsYyVp********************Kj/rPwK8O1XZnrhIbSnLOIekqKTwMA1zK/UqMliMWGCmG1vTLsDwL1GxA3ZRhTopCNYPXF4kE8iQ1TtTTMJHlPNRm4PObPu+6Tqcaj7ajkHuYqkQhVGmZyPfXgC5H4J71GQkf57YnAmDSvUXit7fsaIkb0Gxfx8nOma2EHdSHXe7g8RTD55UGg65rezuyrEKsvh8JzVqycC/cVtu6L4+s21bwR7U+UxBAOXVko0nnUiJCKnRAXu9U41i2vcfPC5/MHYcg/b2HfwYAkqx+fqVnW888FumwWru/jE3aps4b+I4Ci02b63yYgLKc0ShB5WtukfFivSZLRg1a8GW6kpgv5+5+Ntn1KcCWv4MLio/4qsRLi544Of643QFnr8arKMLZrRiAZWBh2xexAVlgyzYdfqdQEwZm5hqpfaU3vUZWx8kqvZ+hZ2/utnsPi0iD5cknz1ewHUMBl7ctz6lqgEp9K/0TFOR+YshpBTWa4HkAO2QSGcVUB83W4TM3B5pOKA/IOX6GZflnbNvsYOYYbvI0e5i3X25enKR7NPaFNljRdOOWEKLfM6CHqMdkAroUlJ57BNTHqRUX7K4ZHE8jfdY93e7LgRWGPhDsihtfuMRKr9T7uh2mCK88bkLncMgRwmhxtOfQUI=
      file: app/build/outputs/apk/app-release.apk
      skip_cleanup: true
      on:
        repo: JianxunRao/MoqiDump
    after_deploy:
    - fir login 5f338b***************5540d9c006 #fir账号的Token
    - fir me #打印身份信息,验证是否登录成功
    - fir p app/build/outputs/apk/app-release.apk #自动发布应用至fir内测平台
```
其中 `api_key:secure`的获取方法是在工程的根目录下执行
>travis setup releases

按照控制台的相应提示,输入GitHub账号密码,指定发布的apk文件路径即可自动完成`deploy:`这一设置项,并自动添加进`.travis.yml`文件中.
完成上述配置后可在GitHub中的设置页面看到如下配置:
![travis-github-personal-access-token](http://cdn.trojx.me/blog_pic/travis-github-personal-access-token.jpg)

`deploy`过程执行完后,即可在GitHub中找到项目的发布内容,如下图:
![git-hub-releases](http://cdn.trojx.me/blog_pic/git-hub-releases.png)
可以看到打包好的apk文件同样被发布了.

在travis完成`deploy`之后,就会执行`after_deploy`中的操作--将打包文件发布至fir中.fir账号的Token这样获取:
![fir-api-token.png](http://cdn.trojx.me/blog_pic/fir-api-token.png)

#### 相关参考
上述内容是基于以下文章的指导而完成的:

- [基于Travis CI搭建Android自动打包发布工作流（支持Github Release及fir.im）](http://avnpc.com/pages/android-auto-deploy-workflow-on-travis-ci)
- [如何简单入门使用 Travis-CI 持续集成 - 掘金](http://gold.xitu.io/entry/57070b048ac247004c0e3d8f)
- [用Travis CI给Android项目部署Github Release · Kesco Lin](http://kescoode.com/travis-ci-android-github-release/)
