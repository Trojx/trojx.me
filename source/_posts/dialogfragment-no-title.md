---
title: DialogFragment去除默认标题栏并横向充满屏幕
date: 2016-10-09 17:07:10
tags: [DialogFragment,Title]
---

这种自定义的对话框的需求还是挺多的,尤其是在需要改变对话框标题的颜色/字体/大小的时候.
设置的方法如下两步:

1.在DialogFragment的onStart方法中添加
``` Java
    @Override
    public void onStart() {
        super.onStart();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics( dm );
        getDialog().getWindow().setLayout( dm.widthPixels, getDialog().getWindow().getAttributes().height );
    }
```

2.在DialogFragment的onCreateView方法中添加
```Java
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_evaluate_order, container, false);
        ButterKnife.bind(this, view);

        //添加这一行
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        initViews();
        return view;
    }
```
效果如图所示
![http://cdn.trojx.me/blog_pic/dialogfragment_no_title.png](http://cdn.trojx.me/blog_pic/dialogfragment_no_title.png)