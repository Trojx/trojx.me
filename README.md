### 个人博客
部署在阿里云OSS上的Hexo静态博客

[http://www.trojx.me](http://www.trojx.me)

#### 部署注意事项
由于阿里云oss(包括其他对象存储服务)不支持访问一个目录时直接定向至访问该目录下的index.html文件,因此需要把所有页面中的链接地址全部
后续加上/index.html

修改方法

1. 修改hexo源码

[参考地址1](https://blog.szlt.net/2017/07/12/hexo-aliyun-oss-cdn.html)

    /node_modules/hexo/lib/plugins/helper/list_archives.js
         if (type === 'monthly') {
           if (item.month < 10) url += '0';
    -      url += item.month + '/';
    +      url += item.month + '/index.html';
         }
    
    /node_modules/hexo/lib/plugins/helper/paginator.js
      function link(i) {
    -    return self.url_for(i === 1 ? base : base + format.replace('%d', i));
    +    return self.url_for(i === 1 ? base : base + format.replace('%d', i) + 'index.html');
       }
       
    /node_modules/hexo/lib/plugins/helper/tagcloud.js
    result.push(
    -      '<a href="' + self.url_for(tag.path) + '" style="' + style + '">' +
    +      '<a href="' + self.url_for(tag.path) + 'index.html' + '" style="' + style + '">' +
           (transform ? transform(tag.name) : tag.name) +
           '</a>'
         );
 

2. 修改Next主题配置

[参考地址2](https://github.com/hexojs/hexo/issues/2021)

[参考地址3](https://github.com/iissnan/hexo-theme-next/issues/604)

    permalink: :year/:title/index.html
    
    menu:
      home: /index.html
      archives: /archives/index.html
      tags: /tags/index.html

#### 部署方式
运行目录下的 aliyun_oss_deploy.py 文件
>python aliyun_oss_deploy.py