#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/12/27 0027 下午 9:46
# @Author  : Trojx
# @File    : aliyun_oss_deploy.py
import hashlib
import json
import os, sys
import oss2

root_path = 'public'
md5_history_path = 'file_md5_history.json'
oss_ak_sk_path = 'aliyun_oss_ak_sk.json'

with open(oss_ak_sk_path, 'r')as f:
            ak_sk = json.load(f)
            ak = ak_sk['AK']
            sk = ak_sk['SK']

auth = oss2.Auth(ak, sk)
bucket = oss2.Bucket(auth, 'oss-cn-hangzhou.aliyuncs.com', 'trojx-blog')

def calc_md5(filepath):
    """
    计算文件MD5
    :param filepath: 
    :return: 
    """
    with open(filepath, 'rb') as f:
        md5obj = hashlib.md5()
        md5obj.update(f.read())
        hash = md5obj.hexdigest()
        return hash


def get_file_md5_history(file_path):
    """
    获取文件历史MD5
    :param file_path: 
    :return: 
    """
    if not os.path.isfile(md5_history_path):
        return None
    else:
        with open(md5_history_path, 'r')as f:
            history = json.load(f)
            if file_path in history:
                return history[file_path]
            else:
                return None

def set_file_md5_history(file_path, file_md5):
    """
    保存文件MD5
    :param file_path: 
    :param file_md5: 
    :return: 
    """
    if not os.path.isfile(md5_history_path):
        with open(md5_history_path, 'w')as f:
            f.write('{}')
    with open(md5_history_path, 'r')as f:
        history = json.load(f)
    with open(md5_history_path, 'w')as f:
        history[file_path] = file_md5
        json.dump(history, f)


def upload_file(file_path, file_name):
    md5_history = get_file_md5_history(file_path)
    md5 = calc_md5(file_path)
    if md5_history is not None and md5_history == md5:
        print('[文件未改动]', filename)
    else:
        with open(file_path, 'rb') as f:
            bucket.put_object(file_name, f)
            set_file_md5_history(file_path, md5)
            print('[文件上传]', filename)


if __name__ == '__main__':
    for f in os.walk(root_path):
        for sub_f in f[2]:
            filepath = os.path.join(f[0], sub_f)
            filename = os.path.join(f[0].replace(root_path, ''), sub_f).replace('\\', '/').replace('/', '', 1)
            upload_file(filepath, filename)
