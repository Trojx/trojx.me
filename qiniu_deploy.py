# coding=utf-8
# Created by Administrator on 2017/5/27
import hashlib, os, json, sys
from qiniu import Auth, put_file, etag, urlsafe_base64_encode
import qiniu.config

AK = 'B-aqeCkTeD6K2wh6uEP5Hdz6RdFEF-WxQ_sSkK9M'
SK = 'cXfSWeQ_x__SYXe9SjmqkIH4jDxJAKKLIgWohEbH'
BUCKET_NAME = 'blog'


def calc_md5(filepath):
    with open(filepath, 'rb') as f:
        md5obj = hashlib.md5()
        md5obj.update(f.read())
        hash = md5obj.hexdigest()
        return hash


def is_file_updated(filepath):
    updated = False
    file_md5 = calc_md5(filepath)
    file_md5_map = {}
    if not os.path.isfile('./file_md5.json'):
        with open('./file_md5.json', 'wb') as f:
            json.dump(file_md5_map, f)
    with open('./file_md5.json', 'r') as f:
        file_md5_map = json.load(f)
        if filepath in file_md5_map:
            if file_md5 == file_md5_map[filepath]:
                print('文件未改动')
            else:
                file_md5_map[filepath] = file_md5
                print('文件改动')
                updated = True
        else:
            file_md5_map[filepath] = file_md5
            print('新文件')
            updated = True
    with open('./file_md5.json', 'w') as f:
        json.dump(file_md5_map, f)
    return updated


def upload_file(filepath,filename):
    if not is_file_updated(filepath):
        return

    q = Auth(AK, SK)
    # 上传到七牛后保存的文件名
    key = filename
    # 生成上传 Token，可以指定过期时间等
    token = q.upload_token(BUCKET_NAME, key, 3600)
    ret, info = put_file(token, key, filepath)
    print(info)
    pass


if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('utf-8')
    root_path = sys.argv[1]
    i=0
    for f in os.walk(root_path):
        for sub_f in f[2]:
            filepath=os.path.join(f[0],sub_f)
            filename=os.path.join(f[0].replace(root_path,''),sub_f).replace('\\','/').replace('/','',1)
            print filepath,filename
            upload_file(filepath,filename)
